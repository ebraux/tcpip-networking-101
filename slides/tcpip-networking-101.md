---
marp: true
paginate: true
theme: default
---
<!-- #3eaff3 bleu ciel -->
<!-- #0074d0 bleu electrique -->
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:40% 60%](assets/network.png)

## TCP/IP Networking 101 

Concepts et principes de base des
réseaux TCP/IP pour les réseaux privés, ou les infrastructures Iaas

-manu_braux@hotmail.com-

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 50%](assets/network.png)

# TCP/IP Networking 101 

**Grands principes de la mise en réseau**
**L'adressage IP : adresses et réseaux**
**Le routage**
**Les services : DNS, DHCP**
**Un peu de théorie**

---

# Licence informations


Auteur : -manu_braux@hotmail.com-

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 50%](assets/network.png)

# TCP/IP Networking 101 

## Mise en garde

---

## Ce cours est : 

- Une approche pratique, simple, et parfois simpliste des grands principes utilisés dans les réseaux privés et Iaas
- Il n'aborde pas les aspects théoriques, ou concepts de bas niveau des réseaux

## Ce cours n'est pas :

- Un cours de réseau à proprement parler
- Il ne s'adresse pas à des personnes souhaitant opérer des réseaux TCP/IP en entreprise, ou passer des certifications.

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 50%](assets/network.png)

# TCP/IP Networking 101 

## Grands principes de la mise en réseau

---
## L'utilisation des réseaux

- Un réseau informatique est un ensemble d'équipements interconnectés (ordinateurs, imprimantes, serveurs, smartphones, ...) qui servent à partager un flux d’informations (messages, informations, fichiers, ...).
- Dans la plupart des cas, l'échange de données se fait entre 2 périphériques : un client et un serveur
- Par exemple
  - Consulter un site internet avec un navigateur
  - Transférer des fichiers sur un espace partagé
  - Se connecter à une machine distante
  - Envoyer des mails
  - ...


---
## Principes de base des réseaux TCP/IP
 
- Chaque périphérique est identifié par un numéro unique : l'adresse IP 

- Un serveur peut fournir des services différents (HTTP/HTTPS, SSH, FTP, ...).
  -  Un "canal de communication" spécifique utilisé pour chaque service : le port de connexion
  -  Un protocole spécifique est utilisé pour dialoguer. C'est un ensemble de règles qui régissent les échanges de données, et le comportement liés au service.
  

![](img/client-serveur-port.jpg)

---
## Aspects "Ergonomie"

- Pour chaque service, le client doit être capable de communiquer avec le protocole utilisé par le service
- Dans la plupart des cas, une application permet de dialoguer sans connaître les détails et la syntaxe du protocole : un navigateur, un client SSH, ... 
- Le numéro de port associé aux principaux protocoles est standardisé : HTTP=80, HTTPS=443, SSH=22, ...
- Pour simplifier la connexion : un nom peut être associé à cette adresse IP : fonctionnalité DNS *(que nous détaillerons plus tard)*

*[https://fr.wikipedia.org/wiki/Liste_de_ports_logiciels](https://fr.wikipedia.org/wiki/Liste_de_ports_logiciels)*

---
## Se connecter à https://www.wikipedia.fr avec un navigateur 

- Définition des informations
  - Adresse IP : résolution DNS -> 51.254.200.228
  - Port : port standard pour les service HTTPS  -> 443
  - Protocole de communication : HTTP
- Echange d'information avec le serveur
  - Connexion au serveur 51.254.200.228, sur le port 443
  - Echange de données avec le protocole HTTP, notament
    - le HOST = www.wikipedia.fr
    - le fichier à afficher : ici la racine du site "/"
---

![bg left:30% 90%](img/InternetProtocolStack.png)

## TCP/IP

Correspond aux protocoles utilisés pour le transfert des données sur Internet
- TCP : Transmission Control Protocol
- IP : Internet Protocol
  
C'est une version simplifié du modèle OSI *(nous y reviendrons plus tard)*

Il existe d'autres protocoles.

[https://fr.wikibooks.org/wiki/Les_r%C3%A9seaux_informatiques/Les_mod%C3%A8les_OSI_et_TCP](https://fr.wikibooks.org/wiki/Les_r%C3%A9seaux_informatiques/Les_mod%C3%A8les_OSI_et_TCP)

---

<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 50%](assets/network.png)

# TCP/IP Networking 101 

## L'adressage IP

---
## L'adresse IP 

"Une adresse IP (avec IP pour Internet Protocol) 
- est un numéro d'identification 
- qui est attribué de façon permanente ou provisoire à chaque périphérique relié à un réseau informatique qui utilise l'Internet Protocol.

L'adresse IP est à la base du système d'acheminement (le routage) des paquets de données sur Internet."

*[https://fr.wikipedia.org/wiki/Adresse_IP](https://fr.wikipedia.org/wiki/Adresse_IP)*

---
## C'est donc

- un numéro unique
- dont une partie permet d'identifier le réseau auquel est connecté l'hôte
- le reste de définir l'adresse de l'hôte sur ce réseau

A partir de l'adresse IP **192.168.0.12**, on peut déduire *(nous verrons comment)* : 
-  qu'il s'agit de l'adresse de l'hôte 12,
-  dans le réseau "192.168.0"


Il existe des adresses IP de version 4, et de version 6. 


---
![bg left:40% 90%](img/Ipv4_address-fr.png)

## Adresse IP V4

- Sur 32 bits
- Généralement représentée en notation décimale avec :
  - quatre nombres compris entre 0 et 255,
  - séparés par des points
- Actuellement la plus utilisée

---
![bg left:50% 80%](img/Ipv6_address-fr.png)

## Adresse IP V6
- sur 128 bits
- IPv6 a été développé en particulier pour palier à la pénurie d’adresses en IPv4

---
## Adresses IP spécifiques

- 127.0.0.1 : boucle locale, 
  - pour utiliser le réseau sur un poste local (LOCALHOST)
  - [https://fr.wikipedia.org/wiki/Localhost](https://fr.wikipedia.org/wiki/Localhost)
- 169.254.169.254 : adresse de lien local,
  - par exemple entre une VM et un système hôte
  - [https://en.wikipedia.org/wiki/Link-local_address](https://en.wikipedia.org/wiki/Link-local_address)



---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 50%](assets/network.png)

# TCP/IP Networking 101 

## Les réseaux IP

---
## Les réseaux IP
- Un réseau informatique permet d'interconnecter des équipements.
  
- Dans un réseau TCP/IP, une adresse IP est attribuée à chaque équipement.

- Chaque adresse IP fait partie d'un réseau

- Il ne peut pas y avoir plusieurs équipements avec la même adresse IP dans un même réseau.

- Dans la pratique, on parle de sous-réseau *(nous verrons pourquoi)*

- Tous les équipements appartenant au même sous-réseau peuvent communiquer ensemble

---
## Structure d'un sous réseau

- Un sous-réseau IP est un ensemble d'adresses IP contiguës

- La première adresse est réservée à la gestion du réseau

- En général : 
    - la dernière est utilisée pour le "Broadcast" (envoi d'un message à toutes les adresses)
    - la 2ème est utilisée comme passerelle vers d'autres réseaux.
---

## Structuration des réseaux : les sous réseaux

Mécanisme de découpage en sous réseau : Utilisation d'un masque
- Même longueur qu'une adresse IP.
- Opération ET logique bit à bit entre l'adresse et le masque.

![bg left:45% 90%](img/masquesousreseau_1.png)

---
## La notation CIDR 

**C**lassless **I**nter **D**omain **R**outing

Objectif : 
- Simplifier la notation des subnets
- Réduire la taille des tables de routage dans les équipements réseau
- On précise le nombre de bits du masque réseau à 1 (en binaire)

Exemples :
- Netmask 255.255.255.0 - `11111111.11111111.11111111.00000000` : 24 bits à 1
  - 192.168.0.0 + netmask 255.255.255.0 =  192.168.0.0/24
- Netmask 255.255.255.192 - `11111111.11111111.11111111.11000000` : 26 bits à 1
  - 192.168.0.0 + netmask 255.255.255.192 =  192.168.0.0/26
  
  
---
## Exemple, pour le réseau 192.168.0.0/24

- 255 adresses
- 254 hôtes possibles
- adresse de broadcast : 192.168.0.255
- adresse de passerelle :  192.168.0.1
- 252 adresse utiles
 
---

## L'adressage privé

- Objectif :
  - déployer des réseaux internes non connectés aux réseaux publiques
  - limiter l'usage des adresses IP publiques
- 3 blocs d'adresses IP privées ont été définis (RFC-1918 - Address Allocation for Private Internets).
  - 10.0.0.0/8
  - 172.16.0.0/12
  - 192.168.0.0/16
- Le plus utilisé est le sous réseau 192.168.0.0/24

---

## Détails des subnet privés

- 10.0.0.0/8
  - plage IP : 10.0.0.0 à 10.255.255.255
  - masque : 255.0.0.0,
  - 16 777 214 hôtes (classe A)
- 172.16.0.0/12
  - plage IP : 172.16.0.0 - 172.31.255.255	
  - masque: 255.240.0.0,
  - 1 048 576 hôtes, ou 255 sous-réseau de 65 536 hôtes (classe B)
- 192.168.0.0/16
  - plage IP : 192.168.0.0 - 192.168.255.255
  - masque: 255.255.0.0,
  -  65 536 hôtes (classe B), ou 255 sous réseau de 256 Hôtes (Classe C)

---
## Structuration des réseaux : classes d'adresses IP

Les adresses réseaux IP étaient réparties en classes (A, B, C, D et E).

- Le nombre d'octets permettait de définir l'adresse des hôtes dépendant de la classe
- La valeur du premier octet permettait d’identifier la classe

  - classe A : 3 octets, soit 16 777 214 hôtes  (CIDR /8)
  - classe B : 2 octets, soit 65 534 hôtes (CIDR /16)
  - classe C : 1 octets, soit 254 hôtes  (CIDR /24)
  - classe D : dédiée aux services de multidiffusion
  - classe E : réservées aux expérimentations

- Système de classe était insatisfaisant: trop rigide, mauvais découpage, mauvaise répartition, ...

---

## Pour l'adresse 192.168.0.12

- "historiquement" c'est une classe C, qui comprend 254 hôtes
    - correspond aux adresses 192.168.0.0 à 192.168.0.255
    - le masque permettant de définir le subnet est 255.255.**255**.0
    - notation CIDR 192.168.0.12/24    

- Si on utilise un masque 255.255.255.**192**, on obtient
     - 62 hôtes
     - adresses de 192.168.0.0 à 192.168.0.62
     - notation CIDR 192.168.0.12/26    
  
*[https://cric.grenoble.cnrs.fr/Administrateurs/Outils/CalculMasque/](https://cric.grenoble.cnrs.fr/Administrateurs/Outils/CalculMasque/)*

---

![](img/detail-classes-IPV4.png)
*[https://www.inetdoc.net/articles/adressage.ipv4/adressage.ipv4.class.html](https://www.inetdoc.net/articles/adressage.ipv4/adressage.ipv4.class.html)*

---

<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 50%](assets/network.png)

# TCP/IP Networking 101 

## Le routage entre sous-réseaux

---
## Problématique

- Par défaut :
    - Les machines peuvent communiquer quand elles sont dans le même sous-réseau.
    - chaque sous réseau est isolé.

- Comment les machines dialoguent d'un réseau à un autre ?


- Ex 192.168.0.12, qui est dans le sous-réseau 192.168.0.0/24
    - Peut dialoguer avec les machines de son réseau: 192.168.0.50 par exemple
    - Ne peut pas dialoguer avec les machines d'un autre réseau : 192.168.**1**.50 par exemple (qui est potentiellement dans le sous-réseau 192.168.1.0/24)

---
## Création d'un pont entre les sous-réseau

- Mise en place d'un lien entre ces 2 sous-réseaux : c'est le travail du routeur
- Chaque sous réseau est connecté au routeur
- Le routeur permet d'aiguiller le dialogue entre les hôtes des sous réseaux qui lui sont reliés.

- Le routeur devient donc un hôte du sous réseau, il obtient une adresse IP dans ce sous réseau.
- Par convention, c'est la 2ème adresse  du sous réseau. On parle de "passerelle".

---
## Pont entre des sous-réseaux connus

Quand l'hote 192.168.0.12, essaye de communiquer avec 192.168.1.50 : 
- "192.168.1.50" n'est pas dans son sous réseau
- la demande est envoyée à la passerelle, et arrive au routeur
- le routeur redirige la demande vers le sous réseau qui contient l'adresse IP 192.168.1.50

---
## Pont entre des sous réseaux non connus

- Si le routeur ne connaît pas l'adresse, la demande va échouer 
- Le routeur peut lui même être connecté à un autre routeur.
  - S'il ne connaît pas l'adresse, il redirige la demande vers cet autre routeur.
  - Et ainsi de suite, jusqu'à ce qu'un routeur détienne l'information sur le sous-réseau cible.

*Rem : les sous-réseau "privés" ne sont pas "routés" sur le réseau public*

---
## Régles de routage

- Table de routage :
  - Toutes les machines (y compris les routeurs) possèdent une table de routage,
  - Une table de routage contient des routes,
  - Une route contient les paramètres pour déterminer par quel routeur ou passerelle passer
- Pour un réseau local :
  - Utilisation des routes configurées,
  - Utilisation de la route par défaut,
- Pour un réseau global :
  - Quel chemin prendre entre deux machines ?
  - Peut-on déterminer le chemin le plus court ?
  - Protocoles : RIP, OSPF, ...
---
## Mise en garde : Recouvrement de sous réseaux (Overlapping Subnets)

- Erreur de d'architecture
  - par exemple, l'adresse 192.168.1.50 peut appartenir aux sous réseaux "192.168.0.0/16" et "192.168.1.0/24"
- Dans le cas d'une interconnexion de réseaux privés, les mêmes sous réseaux peuvent être utilisés des 2 côtés:
  - VPN, multi-cloud, ... 

---
### Multicloud

![h:240](img/overlappingIPs.png)
*[https://aviatrix.com/learn-center/cloud-networking/handling-overlapping-ips/](https://aviatrix.com/learn-center/cloud-networking/handling-overlapping-ips/)*

### VPN

![h:140](img/vpn-overlaping.gif)
*[https://www.juniper.net/documentation/en_US/release-independent/nce/topics/concept/lan2lan-vpn-jseries-srx-series-overview.html](https://www.juniper.net/documentation/en_US/release-independent/nce/topics/concept/lan2lan-vpn-jseries-srx-series-overview.html)*

---
## Mise en garde : Tempête de broacast

![bg left:45% 90%](img/broadcast-storm.png)

- Risque de provoquer une boucle
- Les routeurs doivent être hiérachisés
- Protection : protocole STP (Spannig Tree)
- [https://fr.wikipedia.org/wiki/Spanning_Tree_Protocol](https://fr.wikipedia.org/wiki/Spanning_Tree_Protocol)

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 50%](assets/network.png)

# TCP/IP Networking 101 

## Interconnexion réseaux publics / réseaux privés : NAT
---

## NAT : Network Address Translation 

- "translation d'adresse réseau"
- "faire correspondre des adresses IP à d'autres adresses IP".
- Cas d'usage : 
  - NAT sortant : permettre à des machines disposant d'adresses privées de communiquer avec un réseau public en utilisant vers l'extérieur des adresses externes publiques, uniques et routables. 
  - NAT entrant : rendre accessible sur un réseau public des services s'exécutant sur des machines connectées à un réseau privé

---
![h:500](img/nat-2.jpg.png)

- *[https://ottverse.com/what-is-nat-network-address-translation-webrtc/](https://ottverse.com/what-is-nat-network-address-translation-webrtc/)*
- *[https://fr.wikipedia.org/wiki/Network_address_translation](https://fr.wikipedia.org/wiki/Network_address_translation)*

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 50%](assets/network.png)

# TCP/IP Networking 101 

## Les services : le DHCP

---
## Affectation manuelle des adresses IP

- Adresse IP "fixe", ou "statique"
- Nécessité de configurer chaque périphérique
- Maintenance importante
- Risque d'erreur
- Impose une adresse IP dédiée par équipement, même non actif 
   
---
## Affectation dynamique des adresses IP

- DHCP : Dynamic Host Configuration Protocol.
- Les clients interrogent un serveur, et obtiennent dynamiquement une adresse IP
- L'adresse Ip est affectée : 
  - pour une durée définie : un "bail" qui doit être renouvelé
  - uniquement aux équipements actifs
- La configuration réseau est également diffusée, et peut donc être centralisée :
  - masque de sous réseau
  - passerelle
  - les serveurs DNS,
  - ...


*[https://www.commentcamarche.net/contents/517-le-protocole-dhcp](https://www.commentcamarche.net/contents/517-le-protocole-dhcp)*

---
## Détail du fonctionnement

- Le périphérique client démarre : il n'a aucune information sur sa configuration IP
- Pour joindre le serveur: requète en broadcast **DHCPDISCOVER**
- Réponse du serveur au client, avec une configuration minimale **DHCPOFFER**
- Validation de la configuration par le client **DHCPREQUEST**
- Réponse du serveur, avec la configuration IP pour le client : **DHCPACK**
![h:300](img/dhcp2.png)  

---
## Gestion des adresses IP dynamiques

- Le serveur DHCP dispose d'une plage d'adresses à distribuer : tout ou partie d'un subnet
- La configuration dynamique via DHCP pour un serveur n'est pas recommandée (stabilité)
- On retrouve régulière pour un subnet :
  - une partie sans DHCP pour des serveurs
  - une partie avec DHCP pour les clients
- Pour obtenir des IP "durables"
  - configurer des baux infinis, sans renouvellement
  - forcer l'association des adresses IP aux adresses MAC des périphériques.

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 50%](assets/network.png)

# TCP/IP Networking 101 

## Les services : le DNS


---
## Utiliser des noms

- Utiliser des adresses IP, c'est bien : https://91.198.174.192
- Mais utiliser des noms, c'est mieux : https://www.wikipedia.org

Domain Name System (DNS) = convertir (résoudre) le nom d'un service, en adresse IP

---
## Structure d'un nom DNS

- Chaque partie est séparée par un point.
- Chaque "partie" est appelée "label"
- L'ensemble des labels constituent un FQDN : Fully Qualified Domain Name.
- On travaille de gauche à droite
- Le premier label est appelé Top Level Domain (TLD)
  - il existe des TLD nationaux :fr, it, de, es, ..
  - et les TLD génériques : com, org, net, biz, ...
 - Le deuxième label correspond en général au nom de la société ou du service
 - Le troisième label correspond en général au nom du serveur. Il est optionnel
 - Il est possible d'ajouter d'autres labels

---
## Organisation hiérachique des serveurs
![bg left:50% 90%](img/DNS-Hierachie.png)

- les TLD sont gérés par des serveurs "racine"
- Chaque "label" est ensuite géré par un serveur DNS spécifique

---
## Détail d'une requête DNS

Pour obtenir l'IP du site www.wikipedia.org
- Interrogation du serveur DNS indiqué dans votre configuration DNS.
- Par exemple si il gère des noms DNS en "mycompany.fr", il est lié au serveur racine de ".fr"
- Interrogation du serveur racine de ".fr"
- Redirection vers le serveur racine de ".org"
- Interrogation du serveur racine de ".org" pour connaître le serveur DNS gérant le label "wikipedia"
- Interrogation du serveur DNS gérant le label "wikipedia", pour connaître l'adresse IP correspondant au FQDN "www.wikipedia.org"
  
---
## Gestion des serveurs racine

- Les labels  "Racine" sont gérés par un organisme américain appelé l'ICANN
- Jusque récemment il n'existait que 13 serveurs gérant les labels "Racine"
- Plusieurs attaques : 
  - augmentation du nombre de serveur gérant les labels racine
  - projets pour sécuriser les requêtes DNS

[https://www.icann.org/fr](https://www.icann.org/fr)

---

## Gestion des noms DNS en .fr

- L'ICANN délègue les domaines de premier niveau à divers organismes.
- Pour l'Europe, c'est le RIPE
- le RIPE délègue lui-même à L'AFNIC la gestion des domaines en .fr

 
 [https://fr.wikipedia.org/wiki/Registre_Internet_ré%C3%A9gional](https://fr.wikipedia.org/wiki/Registre_Internet_r%C3%A9gional)
 [https://www.afnic.fr/](https://www.afnic.fr/)

---
## Convention de nommage

- 63 caractères max pour un Label (souvent 12 max par habitude)
- majuscules et minuscules indifférenciées,
- les chiffres, '-' et '_' sont autorisés,
- les espaces, tabulations sont interdits,
- le nom complet fait 255 caractères max *(il y a débat sur ce point ...)*

---
## Contourner le DNS

- Il existe un système de résolution de nom local : le fichier "hosts"
  - Système Linux et MacOS : /etc/hosts
  - Système Windows : c:\windows\System32\drivers\etc\hosts
- Peut contenir des entrées au format "adresse IP" "nom de domaine"
- Utilisé en priorité par le système pour la résolution des adresses IP


---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 50%](assets/network.png)

# TCP/IP Networking 101 

## Exemple d'une Box Internet

---
## Qu'est ce qu'une BOX internet

- Un sous réseau local
  - connecter vos équipements
  - en général, le sous-réseau 192.168.0.0/24
  - le même sous réseau quel que soit le type de connexion : filaire, wifi
  - un serveur DHCP fournissant la configuration réseau aux équipements connectés
- une adresse IP "publique" fournie par le fournisseur d'accès (FAI)
- un routeur, permettant de relier le réseau du FAI, et votre sous-réseau privé
- un mécanisme de NAT, vous permettant de "sortir" sur Internet.

---
![](img/NAT-11.png)
*[http://www.patrickdenis.biz/blog/network-address-translation-nat/](http://www.patrickdenis.biz/blog/network-address-translation-nat/)*

---
![](img/fp1.2_la-box.png)
*[https://www.panoptinet.com/securiser-ma-connexion/fiches-pratiques/quy-a-t-il-dans-ma-box.html](https://www.panoptinet.com/securiser-ma-connexion/fiches-pratiques/quy-a-t-il-dans-ma-box.html)*


---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 50%](assets/network.png)

# TCP/IP Networking 101 

## Un peu de théorie : le modèle OSI

---
## Le modèle OSI : les 7 couches

- modèle OSI : Open Systems Interconnection
- Norme de communication en réseau
- Objectif : normaliser les communications pour garantir un maximum d'évolutivité et d'interopérabilité entre les ordinateurs.
- C'est un modèle théorique : le modèle réellement utilisé est le modèle TCP/IP.

*[https://fr.wikipedia.org/wiki/Mod%C3%A8le_OSI](https://fr.wikipedia.org/wiki/Mod%C3%A8le_OSI)*

---

![bg left:40% 90%](img/OSI_Model_v1.png)
## Modèle en couches
- une couche de niveau N offre des services bien définis à la couche de niveau N+1

---

- Chaque couche résout un certain nombre de problèmes relatifs à la transmission de données,
- Chaque couche est indépendante des autres.
- Chaque couche ne peut communiquer qu'avec une couche adjacente.
- Les couches hautes sont plus proches de l'utilisateur et gèrent des données plus abstraites
- Les couches basses mettent en forme les données afin qu'elles puissent être émises sur un médium physique.


---
## Des couches basses aux couches hautes

- **1- physique** : mise en forme du signal (codage, modulation..) et transmission sur le support de communication (fibre optique, radio...)
- **2- liaison de données** : comment l’information est mise en trames (paquets), comment les paquets sont transportés sur la couche physique, résolution des collisions... ( protocole Ethernet, Token Ring, RNIS, Bluetooth, ...)
- **3- réseaux** : comment les paquets sont acheminés sur le réseau, adressage, commutation, routage... (**protocole IP**, X25, ARP, ICMP, ...)

---

- **4- transport** : fiabilisation du transport de l’information, comment les paquets arrivent sans perte, dans l’ordre, à leur destination (**protocoles TCP**, UDP...)
- **5- session** : synchronisation des communications, gestion des « transactions », correction des erreurs ( ISO 8327 / CCITT X.225, RPC, Netbios, ASP) 1, SMB, AFP)
- **6- présentation** : codage des données applicatives (ASCII, Unicode, MIME, ...)
- **7- application** : les "programmes" et protocoles qui utilisent le réseau ; ex : HTTP (le Web), SMTP (messagerie), FTP (transfert de documents), etc... 

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 50%](assets/network.png)

# TCP/IP Networking 101 

## Un peu de théorie : le modèle TCP/IP

---

![bg left:40% 95%](img/Comparaison_des_modèles_OSI_et_TCP_IP.png)

## Le modèle TCP/IP

- Implémentation du modèle OSI
- Ne reprend pas toutes les couches
- Ne respescte pas toutes les contraintes
- Souvent considéré comme "non compatible OSI"


--- 
## Description des couches
- **1-  couche physique** :
  - Rôle : offrir un support de transmission pour la communication.
  - souvent fusionnée avec la couche 2 
- **2-  couche liaison de données** :
  - Rôle : connecter les machines entre elles sur un réseau local.
- **3- couche réseau / Internet (IP)** : 
  - Rôle : interconnecter les réseaux entre eux.
- **4- couche transport (TCP)**:
  - Rôle : gérer les connexions applicatives.
- **7- couche application**:
  - Rôle : point d'accès aux applications

---

![h:400](img/Data_Flow_of_the_Internet_Protocol_Suite.png)

*[https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet](https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet)*

---
## Protocole IP (Internet Protocol) :

- Gère les adresses
- Contient l'adresse source te l'adresse destination
- Achemine les paquets en fonction de l'adresse destinataire
- Chaque paquet est indépendant
- Gère la fragmentation des paquets
- Correspond à la couche 3 du modèle OSI
- Spécification complète : RFC 791
  
---
## Protocol TCP (Transfer Control Protocol)

- Protocole orienté connexion encapsulé dans IP
- Achemine les paquets vers les services
- Contient le port source et le port destination
- Fournit un service fiable et sécurisé de remise des paquets
  - Effectue des vérifications sur les paquets
  - Exige un accusé de réception des données
- Correspond à la couche 4 du modèle OSI
- Spécification complète : RFC 793
  
---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 50%](assets/network.png)

# TCP/IP Networking 101 

## Pour conclure

---
## Pour reprendre ce qui a été vu précédemment

- Dialoguer au sein d'un même sous-réseau : couche 2 (adresse IP, subnets, hub switch, ...)
- Dialoguer entre sous-réseaux : couche 3  (routeur)
- Faire dialoguer des applications : couche 4 (ports)
- Utiliser des applications : couche 7 (DNS, DHCP, SSH, HTTP, ... )

---
## Sources / Références
 
- [https://www.seekpng.com/ipng/u2y3q8q8o0r5i1w7_icon-for-network-1-global-network-logo-png/](https://www.seekpng.com/ipng/u2y3q8q8o0r5i1w7_icon-for-network-1-global-network-logo-png/)

- Pour aller plus loin avec le DHCP  :  [https://openclassrooms.com/fr/courses/857447-apprenez-le-fonctionnement-des-reseaux-tcp-ip/856923-le-service-dhcp](https://openclassrooms.com/fr/courses/857447-apprenez-le-fonctionnement-des-reseaux-tcp-ip/856923-le-service-dhcp)
- Pour aller plus loin avec le DNS :  [https://openclassrooms.com/fr/courses/857447-apprenez-le-fonctionnement-des-reseaux-tcp-ip/857163-le-service-dns](https://openclassrooms.com/fr/courses/857447-apprenez-le-fonctionnement-des-reseaux-tcp-ip/857163-le-service-dns)
- Le modèle OSI et TCP/IP : [https://openclassrooms.com/fr/courses/857447-apprenez-le-fonctionnement-des-reseaux-tcp-ip/851033-la-creation-dinternet-le-modele-osi](https://openclassrooms.com/fr/courses/857447-apprenez-le-fonctionnement-des-reseaux-tcp-ip/851033-la-creation-dinternet-le-modele-osi)
- Pour un cours plus théorique sur les réseaux : [http://lsc.univ-evry.fr/~didier/webpage/pedagogie/II25/ii25_global.pdf](http://lsc.univ-evry.fr/~didier/webpage/pedagogie/II25/ii25_global.pdf)