# Lab commande Linux pour les réseaux TCP/IP


## Configuration IP

Vérifier la configuration IP 
```bash
ip a
```
```bash

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:1a:4a:16:03:5e brd ff:ff:ff:ff:ff:ff
    inet 10.29.228.78/24 brd 10.29.228.255 scope global dynamic noprefixroute enp3s0
       valid_lft 401sec preferred_lft 401sec
    inet6 fe80::1bd6:102d:962e:a32/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
```
- l0 : interface de loopback : 127.0.0.1
- enp3s0 : correspond à la carte réseau de la machine
  - adresse ipV4 : 10.29.228.78/24 
  - adrsees IPv6 : fe80::1bd6:102d:962e:a32/64

Vérification du bon fonctionnemnet de la pile érseau sur la machine
```bash
ping -c3 127.0.0.1
ping -c3 10.29.228.78
```


## Table de routage

Afficher la table de routage
``` bash
ip route
```

```bash
default via 10.29.228.1 dev enp3s0 proto dhcp metric 100 
10.29.228.0/24 dev enp3s0 proto kernel scope link src 10.29.228.78 metric 100 
169.254.0.0/16 dev enp3s0 scope link metric 1000 
```
- passerelle du subnet :  10.29.228.1
- est également la route par défaut

Vérification de connectivité avec la passerelle
```bash
ping -c3 10.29.228.1
```

Connectivité avec l'extérieur
```bash
ping -c3 www.google.com
```


## DNS

```bash
dig  www.google.com
```

```bash
; <<>> DiG 9.16.1-Ubuntu <<>> www.google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 37976
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;www.google.com.			IN	A

;; ANSWER SECTION:
www.google.com.		161	IN	A	142.250.179.68

;; Query time: 3 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: mar. sept. 07 16:19:45 CEST 2021
;; MSG SIZE  rcvd: 59
```


```bash
nslookup www.google.com
```

```bash
Server:		127.0.0.53
Address:	127.0.0.53#53

Non-authoritative answer:
Name:	www.google.com
Address: 142.250.179.68
Name:	www.google.com
Address: 2a00:1450:4007:813::2004
```

## Routage

```bash
sudo apt install inetutils-traceroute
```

```bash
traceroute www.google.com
```

```bash
traceroute to www.google.com (142.250.179.68), 64 hops max
  1   10.29.228.2  0,462ms  0,315ms  0,269ms 
  2   193.51.188.10  9,021ms  11,811ms  6,326ms 
  3   193.55.204.18  13,522ms  13,150ms  13,504ms 
  4   193.51.177.89  13,527ms  13,219ms  42,618ms 
  5   193.51.180.97  13,540ms  13,061ms  13,551ms 
  6   72.14.214.160  14,889ms  13,131ms  12,996ms 
  7   108.170.244.193  13,253ms  13,324ms  13,438ms 
  8   142.251.49.133  13,204ms  13,121ms  12,888ms 
  9   142.250.179.68  13,231ms  *  * 
}
```


## DHCP

```bash
less /var/lib/dhcp/dhclient.leases
```
```bash

```
lease {
  interface "enp3s0";
  fixed-address 10.29.228.225;
  option subnet-mask 255.255.255.0;
  option routers 10.29.228.1;
  option dhcp-lease-time 600;
  option dhcp-message-type 5;
  option domain-name-servers xxx.xxx.xxx.xxx,xxx.xxx.xxx.xxx;
  option dhcp-server-identifier 10.29.228.2;
  option domain-search "mydomain.fr.";
  option ntp-servers xxx.xxxxxx.xxx;
  option broadcast-address 10.29.228.255;
  option domain-name "mydomain.fr";
  renew 2 2021/09/07 14:24:57;
  rebind 2 2021/09/07 14:29:42;
  expire 2 2021/09/07 14:30:57;
```

```bash
sudo journalctl | grep  DHCP
```

```bash
sept. 07 16:20:56 SYS-Ubuntu20 dhclient[1887]: DHCPDISCOVER on enp3s0 to 255.255.255.255 port 67 interval 3 (xid=0x1030a130)
sept. 07 16:20:57 SYS-Ubuntu20 dhclient[1887]: DHCPOFFER of 10.29.228.225 from 10.29.228.2
sept. 07 16:20:57 SYS-Ubuntu20 dhclient[1887]: DHCPREQUEST for 10.29.228.225 on enp3s0 to 255.255.255.255 port 67 (xid=0x30a13010)
sept. 07 16:20:57 SYS-Ubuntu20 dhclient[1887]: DHCPACK of 10.29.228.225 from 10.29.228.2 (xid=0x1030a130)
```

## Ports

```bash
sudo apt install net-tools
```

```bash
sudo netstat  -ltunp
Connexions Internet actives (seulement serveurs)
Proto Recv-Q Send-Q Adresse locale          Adresse distante        Etat       PID/Program name    
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      1980/systemd-resolv 
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      532/cupsd           
tcp6       0      0 ::1:631                 :::*                    LISTEN      532/cupsd           
udp        0      0 0.0.0.0:5353            0.0.0.0:*                           427/avahi-daemon: r 
udp        0      0 0.0.0.0:36710           0.0.0.0:*                           427/avahi-daemon: r 
udp        0      0 127.0.0.53:53           0.0.0.0:*                           1980/systemd-resolv 
udp        0      0 0.0.0.0:68              0.0.0.0:*                           1887/dhclient       
udp        0      0 0.0.0.0:631             0.0.0.0:*                           510/cups-browsed    
udp6       0      0 :::45854                :::*                                427/avahi-daemon: r 
udp6       0      0 :::5353                 :::*                                427/avahi-daemon: r
```

- 53: Domain Name System (DNS)
- 631 : Internet Printing Protocol (IPP)

``` bash
netstat -lpn
netstat -4 -lpn
```
